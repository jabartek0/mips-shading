#Copyright 2019 Bartłomiej Janowski

#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
#and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# Let's define our vertex:
# [X - 4B] + [Y - 4B] + [R - 1B] + [G - 1B] + [B - 1B] => Total: 11B
# We'll be using BMP coordinates (from bottom right) through the program

.data

# As we'll be switching values between verticies we only use one point in Data segment, rounding up to 36 for 32-bit multiples:
vertices:	.space 36

#storage for all important data:
img_height:	.space 4
img_width:	.space 4
bytes_per_line:	.space 4

#storage for the addresses of arrays:
pixel_array:	.space 4
edge_array:	.space 4



# Header will be stored in a single place. As the lenghth of header is 54B, we need to store 2 first bytes separetly (54 is not multiple of 4)
header:	.space 52


#storage for temporary vertex values
topX:	.space 4
topY:	.space 4
botX:	.space 4
botY:	.space 4

currX:	.space 4

topR:	.space 1
topG:	.space 1
topB:	.space 1

botR:	.space 1
botG:	.space 1
botB:	.space 1

edgeCounter:	.space 1

header_start:	.ascii "BM"

input:	.asciiz	"input.bmp"
output:	.asciiz	"output.bmp"

open_error:	.asciiz "Can't open input file :("
input_error:	.asciiz "Value out of bounds. Exiting :("


load_success1:	.asciiz "File loaded!\n\nWidth: "
load_success2:	.asciiz "\nHeight: "


enterV1:	.asciiz "\nEnter data for V1 (X - Y - R - G - B):\n"
enterV2:	.asciiz "\nEnter data for V2 (X - Y - R - G - B):\n"
enterV3:	.asciiz "\nEnter data for V3 (X - Y - R - G - B):\n"

.text
.globl	main

main:

#opening the file

	li	$v0, 13
	la	$a0, input
	li	$a1, 0	
	li	$a2, 0
	syscall
	move	$t0, $v0 #storing descriptor in t0 register
	
	bltz	$v0, file_fail #end if failed to open
	
	#skipping 2 first bytes ("BM")
	li	$v0, 14
	move	$a0, $t0
	la	$a1, header
	li	$a2, 2
	syscall
	
	#loading the rest of header
	li	$v0, 14
	move	$a0, $t0
	la	$a1, header
	li	$a2, 52
	syscall
	
	#the rest of upper s registers will be used for image parameters
	lw	$t7, header+32 #pixel array size
	lw	$s7, header+16 	#width
	lw	$s6, header+20 	#height
	div 	$s5, $t7, $s6
	mul	$t6, $s7, 3
	#sub 	$s5, $s5, $t6
	
	
	#allocation for pixel array
	li	$v0, 9
	move	$a0, $t7
	syscall
	move	$s0, $v0 #memory address
	
	#loading pixels into array
	li	$v0, 14
	move	$a0, $t0
	la	$a1, ($s0)
	move	$a2, $t7
	syscall
	
	#closing the file
	li	$v0, 16
	move	$a0, $t0
	syscall
	
#	To sum up:
#
#	$s0 - address of the pixel array
#
#	$s7 - image width
#	$s6 - image height
#	$s5 - bytes per line
#
# Storing those values in allocated space, just in case we need the registers:
	sw $s5, bytes_per_line
	sw $s6, img_height
	sw $s7, img_width
	
	sw $s0, pixel_array

#printing image data:

	li	$v0, 4
	la	$a0, load_success1
	syscall
	li	$v0, 1
	move	$a0, $s7
	syscall
	
	li	$v0, 4
	la	$a0, load_success2
	syscall
	li	$v0, 1
	move	$a0, $s6
	syscall

#loading data from the user:

	#vertex 1:
	li	$v0, 4
	la	$a0, enterV1
	syscall
	#X value:
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s7, user_fail
	subi	$v0, $v0, 1	
	sw	$v0, vertices
	#Y value:	
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s6, user_fail
	sub	$v0, $s6, $v0	
	#subi 	$v0, $v0, 1
	sw	$v0, vertices+4
	
	
	#R value:
	li	$v0, 5
	syscall
	sb	$v0, vertices+8
	#G value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+9
	#B value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+10
	
	
	#vertex 2:
	li	$v0, 4
	la	$a0, enterV2
	syscall
	
	#X value:
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s7, user_fail
	subi	$v0, $v0, 1
	sw	$v0, vertices+12
	#Y value:	
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s6, user_fail
	sub	$v0, $s6, $v0	
	#subi 	$v0, $v0, 1
	sw	$v0, vertices+16
	
	
	#R value:
	li	$v0, 5
	syscall
	sb	$v0, vertices+20
	#G value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+21
	#B value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+22
	
	
	#vertex 3:
	li	$v0, 4
	la	$a0, enterV3
	syscall
	#X value:
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s7, user_fail
	subi	$v0, $v0, 1
	sw	$v0, vertices+24
	#Y value:	
	li	$v0, 5
	syscall
	blez 	$v0, user_fail
	bgt 	$v0, $s6, user_fail
	sub	$v0, $s6, $v0	
	#subi 	$v0, $v0, 1
	sw	$v0, vertices+28
	
	
	#R value:
	li	$v0, 5
	syscall
	sb	$v0, vertices+32
	#G value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+33
	#B value:	
	li	$v0, 5
	syscall
	sb	$v0, vertices+34
	
#array for left- and rightmost coords

	lw $t0, img_height
	sll $t0, $t0, 3
	
	#allocation for the array
	li	$v0, 9
	move	$a0, $t0
	syscall
	move	$s1, $v0
	#array allocated at s1
	
	sw $s1, edge_array

	li $s0, 0
	lw $s4, img_height
	
edge_array_loop:
	li $t1, -1
	sll $t2, $s0, 3
	addi $t3, $t2, 4
	add $t2, $s1, $t2
	add $t3, $s1, $t3
	
	sw $t1, ($t2)
	sw $t1, ($t3)
	
	addi $s0, $s0, 1
	 
	ble $s0, $s4, edge_array_loop

#drawing the edges:
li $t0, 0
sb $t0, edgeCounter

draw_edges:
	lb $t0, edgeCounter
	addi $t0, $t0, 1
	sb $t0, edgeCounter
	
	beq $t0, 1, draw_v1v2
	beq $t0, 2, draw_v2v3
	beq $t0, 3, draw_v1v3
	b fill
draw_v1v2:
	lw $t1, vertices+4
	lw $t2, vertices+16
	sgt $t3, $t1, $t2
	sle $t4, $t1, $t2
	mul $t3,  $t3, 12
	mul $t4, $t4, 12
	
	lw $t0, vertices($t3)
	sw $t0, botX
	lw $t0, vertices+4($t3)
	sw $t0, botY
	lw $t0, vertices($t4)
	sw $t0, topX
	lw $t0, vertices+4($t4)
	sw $t0, topY
	
	lb $t0, vertices+8($t3)
	sb $t0, botR
	lb $t0, vertices+9($t3)
	sb $t0, botG
	lb $t0, vertices+10($t3)
	sb $t0, botB
	lb $t0, vertices+8($t4)
	sb $t0, topR
	lb $t0, vertices+9($t4)
	sb $t0, topG
	lb $t0, vertices+10($t4)
	sb $t0, topB
	
	b draw_edge
	
draw_v2v3:
	lw $t1, vertices+16
	lw $t2, vertices+28
	sgt $t3, $t1, $t2
	sle $t4, $t1, $t2
	mul $t3,  $t3, 12
	mul $t4, $t4, 12
	
	lw $t0, vertices+12($t3)
	sw $t0, botX
	lw $t0, vertices+16($t3)
	sw $t0, botY
	lw $t0, vertices+12($t4)
	sw $t0, topX
	lw $t0, vertices+16($t4)
	sw $t0, topY
	
	lb $t0, vertices+20($t3)
	sb $t0, botR
	lb $t0, vertices+21($t3)
	sb $t0, botG
	lb $t0, vertices+22($t3)
	sb $t0, botB
	lb $t0, vertices+20($t4)
	sb $t0, topR
	lb $t0, vertices+21($t4)
	sb $t0, topG
	lb $t0, vertices+22($t4)
	sb $t0, topB
	
	b draw_edge
	
draw_v1v3:
	lw $t1, vertices+4
	lw $t2, vertices+28
	sgt $t3, $t1, $t2
	sle $t4, $t1, $t2
	mul $t3,  $t3, 24
	mul $t4, $t4, 24
	
	lw $t0, vertices($t3)
	sw $t0, botX
	lw $t0, vertices+4($t3)
	sw $t0, botY
	lw $t0, vertices($t4)
	sw $t0, topX
	lw $t0, vertices+4($t4)
	sw $t0, topY
	
	lb $t0, vertices+8($t3)
	sb $t0, botR
	lb $t0, vertices+9($t3)
	sb $t0, botG
	lb $t0, vertices+10($t3)
	sb $t0, botB
	lb $t0, vertices+8($t4)
	sb $t0, topR
	lb $t0, vertices+9($t4)
	sb $t0, topG
	lb $t0, vertices+10($t4)
	sb $t0, topB
	
	b draw_edge

#we assume that $t0/$t2 are the values of LOWER vertex
draw_edge:
	lw $t0, botX
	lw $t1, topX
	lw $t2, botY
	lw $t3, topY
	
	
	sub $s0, $t1, $t0 #horizontal
	sub $s1, $t3, $t2 #vertical
	div $s0, $s1
	mflo $s2 #coeff
	mfhi $s3 #coeff remainder
	
	move $s4, $t2
	
draw_edge_loop:
#protected registers: t0,1,2,3; s1,2,3,4
	lw $t0, botX
	lw $t1, topX
	lw $t2, botY
	lw $t3, topY

	sub $t7, $s4, $t2
	move $s6, $t7 		#distance from bottom vertex
	sub $s5, $t3, $s4 	#distance from top vertex
	sub $s7, $t3, $t2	#vertex to vertex
	mul $t8, $t7, $s2
	mul $t9, $t7, $s3
	div $t9, $s1
	mflo $t5
	add $t8, $t8, $t5
	mfhi $t5
	sll $t5, $t5, 1
	div $t5, $s1
	mflo $t5
	add $t8, $t8, $t5
	add $t8, $t8, $t0
	
	sw $t8, currX
	
	mul $t8, $t8, 3
	lw $t9, bytes_per_line
	mul $t9, $s4, $t9
	
	add $t9, $t8, $t9
	lw $t7, pixel_array
	add $t7, $t7, $t9
	
	lbu $t5, botR
	lbu $t6, topR
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	
	sb $t5, 2($t7)
	
	lbu $t5, botG
	lbu $t6, topG
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	
	sb $t5, 1($t7)
	
	lbu $t5, botB
	lbu $t6, topB
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	sb $t5, ($t7)
	
	lw $s0, currX
	lw $t0, edge_array
	sll $s6, $s4, 3
	add $s7, $t0, $s6
	lw $s5, ($s7)
	
	beq $s5, -1, no_edge
	bge $s0, $s5, curr_greater
	blt $s0, $s5, curr_smaller
	j draw_edge_cont
	
curr_smaller:
	#in this case we need to swap value to 2nd slot of array, than put new value to slot 1
	lw $s6, 4($s7)
	bge $s6, $s5, draw_edge_cont
	sw $s5, 4($s7)
	sw $s0, ($s7)
	j draw_edge_cont
curr_greater:
	#in this case we just put currX into 2nd slot if empty
	lw $s5, 4($s7)
	bgt $s5, $s0, draw_edge_cont
	sw $s0, 4($s7)
	j draw_edge_cont
no_edge:
	#in this case we just put currX into 1st slot
	sw $s0, ($s7)

draw_edge_cont:
	
	lw $t3, topY
	beq $s4, $t3, draw_edges
	
	addi $s4, $s4, 1
	b draw_edge_loop
	
fill:
	li $s0, -1
fill_loop:
	addi $s0, $s0, 1
	sll $t0, $s0, 3
	lw $t1, edge_array
	add $t1, $t0, $t1
	lw $t0, ($t1)
	lw $t2, img_height
	bge $s0, $t2, save
	lw $s2, 4($t1)
	move $s1, $t0
	bltz $s1, fill_loop
	bltz $s2, fill_loop
	beq $s1, $s2, fill_loop	
	lw $t7, bytes_per_line
	mul $t6, $s0, $t7
	mul $t7, $s2, 3
	mul $t5, $s1, 3
	add $t7, $t7, $t6
	add $t6, $t5, $t6
	lw $t0, pixel_array
	add $t7, $t0, $t7
	add $t6, $t0, $t6
	lbu $t1, 2($t6)
	sb $t1, botR
	lbu $t1, 2($t7)
	sb $t1, topR
	lbu $t1, 1($t6)
	sb $t1, botG
	lbu $t1, 1($t7)
	sb $t1, topG
	lbu $t1, ($t6)
	sb $t1, botB
	lbu $t1, ($t7)
	sb $t1, topB
	
	sub $s3, $s2, $s1
	move $s4, $s1
fill_loop_loop:

	sub $s5, $s2, $s4
	sub $s6, $s4, $s1
	sub $s7, $s2, $s1
	mul $t8, $s4, 3
	lw $t9, bytes_per_line
	mul $t9, $s0, $t9
	
	add $t9, $t8, $t9
	lw $t7, pixel_array
	add $t7, $t7, $t9
	
	lbu $t5, botR
	lbu $t6, topR
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	
	sb $t5, 2($t7)
	
	lbu $t5, botG
	lbu $t6, topG
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	
	sb $t5, 1($t7)
	
	lbu $t5, botB
	lbu $t6, topB
	
	mul $t5, $t5, $s5
	mul $t6, $t6, $s6
	add $t5, $t5, $t6
	div $t5, $s7
	mflo $t5
	sb $t5, ($t7)

	addi $s4, $s4, 1
	
	ble $s4, $s2, fill_loop_loop
	
	sll $t0, $s0, 3
	lw $t1, edge_array
	add $t1, $t0, $t1
	lw $t0, ($t1)
	bgez $t0, fill_loop

save:
	li	$v0, 13
	la	$a0, output
	li	$a1, 1
	li	$a2, 0
	syscall
	move	$t0, $v0
	
	li	$v0, 15
	move	$a0, $t0
	la	$a1, header_start 
	li	$a2, 2
	syscall
	
	li	$v0, 15
	move	$a0, $t0
	la	$a1, header
	li	$a2, 52
	syscall
	
	li	$v0, 15
	move	$a0, $t0
	lw	$a1, pixel_array
	lw	$a2, header+32
	syscall
	
	li	$v0, 16
	move	$a0, $t0
	syscall	
		
prog_end:
	li	$v0, 10
	syscall

user_fail:
	li	$v0, 4
	la	$a0, input_error
	syscall
	b prog_end

file_fail:
	li	$v0, 4
	la	$a0, open_error
	syscall
	b prog_end
